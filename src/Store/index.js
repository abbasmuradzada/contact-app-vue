import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        contactList: [
            {
                id:1,
                name: 'Abbas Muradzada',
                number: 557580680,
                createdDate: '14:30',
                edited: false
            },
            {
                id:2,
                name: 'Elmar Gasimov',
                number: 554131836,
                createdDate: '15:40',
                edited: true
            },
            {
                id:3,
                name: 'Semyon Penziyev',
                number: 553654329,
                createdDate: '16:10',
                edited: false
            }
        ],
        editedIndex : null,
        lastIndex : 3
    },
    mutations: {
        // deleteContactMut(state, value){
        //     state.contactList.find((element, index) => {
        //         if (element.id == value) {
        //             state.contactList.splice(index, 1)
        //             localStorage.setItem("contactVue", JSON.stringify(state.contactList));     
        //         }
        //     })
        // }
        // addContactMut(state, newContact){
        //     state.contactList.push({
        //         id: state.lastIndex + 1,
        //         name: newContact.name,
        //         number: newContact.number,
        //         createdDate: newContact.date,
        //         edited: false
        //     })
        //     state.lastIndex++;
        //     localStorage.setItem("lastIndex", JSON.stringify(state.lastIndex));            
        //     localStorage.setItem("contactVue", JSON.stringify(state.contactList));
        // },
        // goToHomeMut(){
        //     this.$router.push({name: 'contact-list'})
        // }
    }
})