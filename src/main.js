import Vue from 'vue'
import App from './App.vue'
import {store} from './Store'
import VueRouter from 'vue-router';
import {routes} from './routes/index'

import vuetify from './plugins/vuetify' // path to vuetify export
import '../node_modules/vuetify/dist/vuetify.min.css';

new Vue({
  vuetify,
}).$mount('#app')

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  vuetify,
  el: '#app',
  store,
  router,
  render: h => h(App)
})
