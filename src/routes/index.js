import ContactList from '../components/ContactList'
import Add from '../components/Add'
import Edit from '../components/Edit'

export const routes = [
    {path: '/', redirect: {name : 'contact-list'} },
    { path : '/contact-list', component: ContactList, name: 'contact-list' },
    { path : '/add', component: Add, name: 'add' },
    { path : '/edit', component: Edit, name: 'edit' },
]

// export const routes = [
//     {path: '/', redirect: {name : 'home'} },
//     {path: '/home', component : Home, name: 'home'},
//     {path: '/blog', component : Blog, name: 'blog'},
// ]