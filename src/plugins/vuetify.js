import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
// import Vuetify from 'vuetify/lib'

const opts = {}

export default new Vuetify(opts)